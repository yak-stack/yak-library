package yakstack.yaklib.Obj

data class ExtendedToken(val returnCode: ReturnCode, val token: Token)

data class Token(val token: String)
