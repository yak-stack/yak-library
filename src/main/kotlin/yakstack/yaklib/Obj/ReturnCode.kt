package yakstack.yaklib.Obj

/**
 * Possible messages the server may send back to the user.
 */
enum class ReturnCode {
    USER_ALREADY_EXISTS,
    USER_DOES_NOT_EXIST,
    INVALID_TOKEN,
    INVALID_PASSWORD,
    BAD_DATE_FORMAT,
    UNKNOWN_ERROR,
    SUCCESS
}
