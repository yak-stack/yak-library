package yakstack.yaklib.Obj

data class CommitRequestRange(
    val email: String,
    val token: Token,
    val startDate: String,
    val endDate: String
)
