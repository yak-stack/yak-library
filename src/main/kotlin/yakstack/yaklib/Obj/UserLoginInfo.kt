package yakstack.yaklib.Obj

data class ExtendedUserLoginInfo(
    val returnCode: ReturnCode,
    val userLoginInfo: UserLoginInfo
)

data class UserLoginInfo(
    val token: Token,
    val email: String,
    val first_name: String,
    val last_name: String
)

data class UserGivenInfo @JvmOverloads constructor(
    val email: String,
    val password: String,
    val first_name: String? = null,
    val last_name: String? = null,
    val device_name: String? = null
)
