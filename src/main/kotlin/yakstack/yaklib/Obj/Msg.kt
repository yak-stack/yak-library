package yakstack.yaklib.Obj

/**
 * Simple message and return code sent back from the server.
 * @param returnCode Response sent back by the server.
 * @param msg Message sent back from the server.
 * @since 1.0-ALPHA
 */
data class ExtendedMsg(val returnCode: ReturnCode, val msg: Msg)

/**
 * Simple message sent back from the server.
 * @param msg Message sent back from the server.
 * @since 1.0-ALPHA
 */
data class Msg(val msg: String)
