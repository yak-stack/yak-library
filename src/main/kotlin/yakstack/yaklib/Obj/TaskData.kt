package yakstack.yaklib.Obj

import yakstack.yaklib.Raw.RawTask
import yakstack.yaklib.Raw.RawTaskData

/**
 * TaskData with a ReturnCode included.
 * @param returnCode http return code.
 * @param taskData User's task data.
 */
data class ExtendedTaskData(
    val returnCode: ReturnCode,
    val taskData: SendTaskData
)

/**
 * TaskData meant to be sent by the user to be consumed by the server.
 * @param token User token.
 * @param tasks User's tasks.
 */
data class TaskData(
    val token: Token,
    val tasks: List<Task>
) {
    internal fun asRawTaskData(): RawTaskData =
        RawTaskData(
            token,
            tasks.asRawTask()
        )
}

/**
 * TaskData meant to be sent by the server to be consumed by the user.
 * @param msg Message from server.
 * @param taskBundle User's tasks and other metadata passed from the server.
 */
data class SendTaskData(val msg: Msg, val taskBundle: TaskBundle)

/**
 * List of tasks sent from the server and other metadata bundled along.
 * @param containsFirst Whether or not the bundle contains the user's last commit.
 * @param tasks User's TimeCommits.
 */
data class TaskBundle(val tasks: List<SendTask>)

fun List<Task>.asRawTask(): List<RawTask> =
    this.map { it.asRawTask() }

/**
 * A simple task.
 * @param category Category of the task.
 * @param task The task itself.
 * @param state State of the task (generally TODO/DONE)
 * @param commits Optional set of commits to be sent along with tasks.
 * @param notes Optional set of notes to be sent along with tasks.
 * @param properties A set of user defined properties relating to the task.
 */
open class Task(
    val category: String,
    val task: String,
    val state: String?,
    val commits: List<TimeCommit>?,
    val notes: String?,
    val properties: Map<String, String>?
) {
    internal fun asRawTask(): RawTask =
        RawTask(
            category,
            task,
            state,
            commits?.asRawTimeCommits(),
            notes,
            properties
        )
}

/**
 * A simple task returned from the server.
 * @param category Category of the task.
 * @param task The task itself.
 * @param properties A set of user defined properties relating to the task.
 * @param commits User's TimeCommits.
 * @param taskId Unique identifier of task.
 */
class SendTask(
    val category: String,
    val task: String,
    val state: String?,
    val commits: List<SendTimeCommit>?,
    val notes: String?,
    val properties: Map<String, Properties>?,
    val taskId: String?
)

/**
 * A single custom user defined property.
 * @param prop The property itself.
 * @param pid A unique identifier of the property.
 */
data class Properties(val prop: String, val pid: String?)
