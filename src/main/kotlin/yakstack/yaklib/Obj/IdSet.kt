package yakstack.yaklib.Obj

/**
 * Return code and set of TaskIds.
 * @param returnCode Return code from server.
 */
data class ExtendedTaskIdSet(val returnCode: ReturnCode, val taskIdSet: TaskIdSet)

/**
 * Set of TaskIds.
 * @param msg Message from the server.
 * @param taskIds Unique task identifiers and the accompanying property identifiers.
 */
data class TaskIdSet(val msg: Msg, val taskIds: List<TaskId>)

/**
 * Set of TaskIds.
 * @param tid Unique identifier for a task.
 * @param propertyIds Unique identifiers for set of properties.
 */
data class TaskId(val tid: String, val propertyIds: List<PropId>)

data class ExtendedTimeCommitIdSet(
    val returnCode: ReturnCode,
    val timeCommitIdSet: TimeCommitIdSet
)

/**
 * Set of time TimeCommitIds.
 * @param timeCommitIds Set of timeCommitIds.
 */
data class TimeCommitIdSet(val timeCommitIds: List<TimeCommitId>)

/**
 * Unique identifier for a task.
 * @param timeCommitId Unique identifier for a task.
 */
data class TimeCommitId(val timeCommitId: String)

/**
 * Unique identifier for a property.
 * @param propId Unique identifier for a property.
 */
data class PropId(val propId: String)
