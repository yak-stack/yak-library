package yakstack.yaklib.Obj

import yakstack.yaklib.Raw.RawTimeCommit
import yakstack.yaklib.Raw.RawTimeCommitData
import yakstack.yaklib.Models.DATE_FORMAT
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * A bundle of TimeCommits meant to be created by the user.
 * @param token: User access token.
 * @param commits: User's TimeCommits.
 */
data class TimeCommitData(
    val token: Token,
    val commits: List<TimeCommit>
) {
    fun asRawTimeCommitData(): RawTimeCommitData =
        RawTimeCommitData(
            token,
            commits.asRawTimeCommits()
        )
}

/**
 * A bundle of TimeCommits sent from the server.
 * @param commitId Commit ID.
 * @param commits TimeCommits.
 */
data class SendTimeCommitData(
    val commitId: String,
    val commits: List<SendTimeCommit>
)

/**
 * A simple TimeCommit.
 * @param startTimeStamp When the task was started.
 * @param endTimeStamp When the user clocks out of the task.
 */
data class TimeCommit(
    val taskId: String,
    val startTimeStamp: ZonedDateTime,
    val endTimeStamp: ZonedDateTime
) {
    fun asRawTimeCommit(): RawTimeCommit {
        return RawTimeCommit(
            taskId,
            startTimeStamp.toFormattedDate(),
            endTimeStamp.toFormattedDate()
        )
    }
}

/**
 * Helper extension function to convert TimeCommits to raw serializable TimeCommits.
 * @return List of raw serializable TimeCommits.
 */
fun List<TimeCommit>.asRawTimeCommits(): List<RawTimeCommit> =
    this.map { it.asRawTimeCommit() }

/**
 * TimeCommit received from the server.
 * @param startTimeStamp When the task was started.
 * @param endTimeStamp When the user clocks out of the task.
 * @param commitId id of time commit.
 */
data class SendTimeCommit(
    val startTimeStamp: ZonedDateTime,
    val endTimeStamp: ZonedDateTime,
    val commitId: String?
)

/**
 * Convert ZonedDateTime to serializable format.
 */
fun ZonedDateTime.toFormattedDate(): String {
    return DateTimeFormatter.ofPattern(
        DATE_FORMAT
    ).format(this)
}
