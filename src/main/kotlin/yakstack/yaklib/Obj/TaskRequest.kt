package yakstack.yaklib.Obj

data class TaskRequest(
    val token: Token,
    val fat: Boolean
)
