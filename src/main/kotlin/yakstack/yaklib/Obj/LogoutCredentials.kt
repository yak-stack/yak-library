package Models

import yakstack.yaklib.Obj.Token

data class LogoutCredentials(
    val token: Token,
    val email: String
)
