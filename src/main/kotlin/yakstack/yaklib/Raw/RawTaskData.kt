package yakstack.yaklib.Raw

import yakstack.yaklib.Obj.Msg
import yakstack.yaklib.Obj.SendTask
import yakstack.yaklib.Obj.SendTaskData
import yakstack.yaklib.Obj.Properties
import yakstack.yaklib.Obj.TaskBundle
import yakstack.yaklib.Obj.Token

/**
 * Raw serializable task data meant to be sent to the server.
 * @param token User token
 * @param tasks User's tasks.
 */
data class RawTaskData(
    val token: Token,
    val tasks: List<RawTask>
)

/**
 * Raw serializable TaskData meant to be sent by the server to be consumed by the user.
 * @param msg Message from server.
 * @param taskBundle User's tasks and other metadata passed from the server.
 */
data class RawSendTaskData(
    val msg: Msg,
    val taskBundle: RawTaskBundle
) {
    fun asSendTaskData(): SendTaskData =
        SendTaskData(
            msg,
            taskBundle.asTaskBundle()
        )
}

/**
 * Raw serializable list of tasks sent from the server and other metadata bundled along.
 * @param containsFirst Whether or not the bundle contains the user's last commit.
 * @param tasks User's TimeCommits.
 */
data class RawTaskBundle(
    val tasks: List<RawSendTask>
) {
    fun asTaskBundle() =
        TaskBundle(
            tasks.asSendTasks()
        )
}

/**
 * Raw serializable task.
 * @param category Category of the task.
 * @param task The task itself.
 * @param state State of the task (generally TODO/DONE)
 * @param commits Optional set of commits to be sent along with tasks.
 * @param notes Optional set of notes to be sent along with tasks.
 * @param properties A set of user defined properties relating to the task.
 */
data class RawTask(
    val category: String,
    val task: String,
    val state: String?,
    val commits: List<RawTimeCommit>?,
    val notes: String?,
    val properties: Map<String, String>?
)

/**
 * Helper extension function to convert list of RawSendTask to SendTask.
 * @return List of SendTask.
 */
fun List<RawSendTask>.asSendTasks(): List<SendTask> =
    this.map { it.asSendTask() }

/**
 * Raw serialized version of a task returned from the server.
 * @param category Category of the task.
 * @param task The task itself.
 * @param commits User's TimeCommits.
 * @param properties A set of user defined properties relating to the task.
 * @param taskId Unique identifier of task.
 */
class RawSendTask(
    val category: String,
    val task: String,
    val state: String?,
    val commits: List<RawSendTimeCommit>?,
    val notes: String?,
    val properties: Map<String, Properties>?,
    val taskId: String?
) {
    fun asSendTask(): SendTask =
        SendTask(
            this.category,
            this.task,
            this.state,
            this.commits?.asSendTimeCommitList(),
            this.notes,
            this.properties,
            this.taskId
        )
}
