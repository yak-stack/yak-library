package yakstack.yaklib.Raw

import yakstack.yaklib.Models.DATE_FORMAT
import yakstack.yaklib.Obj.SendTimeCommit
import yakstack.yaklib.Obj.TimeCommit
import yakstack.yaklib.Obj.Token
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * Raw serializable bundle of TimeCommits.
 * @param token: User access token.
 * @param commits: User's TimeCommits.
 */
data class RawTimeCommitData(
    val token: Token,
    val commits: List<RawTimeCommit>
)

/**
 * Raw serializable TimeCommit.
 * @param startTimeStamp When the task was started.
 * @param endTimeStamp When the user clocks out of the task.
 */
data class RawTimeCommit(
    val taskId: String,
    val startTimeStamp: String,
    val endTimeStamp: String
) {
    /**
     * Helper method to convert RawTimeCommit to TimeCommit.
     * @return User friendly TimeCommit.
     */
    fun asTimeCommit() =
        TimeCommit(
            this.taskId,
            this.startTimeStamp.toZonedDateTime(),
            this.endTimeStamp.toZonedDateTime()
        )
}

/**
 * Raw serializable TimeCommit received from the server.
 * @param startTimeStamp When the task was started.
 * @param endTimeStamp When the user clocks out of the task.
 * @param commitId id of time commit.
 */
data class RawSendTimeCommit(
    val startTimeStamp: String,
    val endTimeStamp: String,
    val commitId: String?
) {
    fun asSendTimeCommit(): SendTimeCommit =
        SendTimeCommit(
            startTimeStamp.toZonedDateTime(),
            endTimeStamp.toZonedDateTime(),
            commitId
        )
}

/**
 * Helper extension function to convert from String to ZonedDateTime.
 */
fun String.toZonedDateTime(): ZonedDateTime {
    return ZonedDateTime.parse(
        this,
        DateTimeFormatter.ofPattern(DATE_FORMAT)
    )
}

/**
 * Helper extension function to convert from raw serializable SendTimeCommit to user friendly version.
 * @return List of user friendly SendTimeCommits.
 */
fun List<RawSendTimeCommit>.asSendTimeCommitList() =
    this.map { it.asSendTimeCommit() }
