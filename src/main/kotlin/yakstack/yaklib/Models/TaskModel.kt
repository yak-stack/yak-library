package yakstack.yaklib.Models

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import yakstack.yaklib.Raw.RawSendTaskData
import yakstack.yaklib.Raw.RawTaskData

import yakstack.yaklib.Obj.TaskRequest
import yakstack.yaklib.Obj.TaskIdSet

internal interface TaskModel {
    @POST("/tasks/add")
    @Headers("Content-Type: application/json")
    fun pushTask(@Body taskData: RawTaskData): Call<TaskIdSet>

    @POST("/tasks/get/short")
    @Headers("Content-Type: application/json")
    fun getTasks(@Body taskRequest: TaskRequest): Call<RawSendTaskData>

    @POST("/tasks/get/full")
    @Headers("Content-Type: application/json")
    fun getTasksAndMetadata(@Body taskRequest: TaskRequest): Call<RawSendTaskData>

    // TODO: Add edit and delete
}
