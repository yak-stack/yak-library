package yakstack.yaklib.Models

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

import yakstack.yaklib.Obj.Msg
import yakstack.yaklib.Obj.Token
import yakstack.yaklib.Obj.UserLoginInfo
import yakstack.yaklib.Obj.UserGivenInfo

interface UserModel {
    @POST("/user/register")
    @Headers("Content-Type: application/json")
    fun registerUser(
        @Body userRegisterInfo: UserGivenInfo
    ): Call<Msg>

    @POST("/user/login")
    @Headers("Content-Type: application/json")
    fun loginUser(
        @Body userGivenInfo: UserGivenInfo
    ): Call<UserLoginInfo>

    @POST("/user/logout")
    @Headers("Content-Type: application/json")
    fun logoutUser(
        token: Token,
        email: String
    ): Call<Msg>
}
