package yakstack.yaklib.Models

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import yakstack.yaklib.Raw.RawTimeCommitData
import yakstack.yaklib.Obj.TimeCommitIdSet

internal interface CommitModel {
    @POST("/commits/add")
    @Headers("Content-Type: application/json")
    fun pushTimeCommit(@Body timeCommit: RawTimeCommitData): Call<TimeCommitIdSet>

    // TODO: Add edit and delete
}