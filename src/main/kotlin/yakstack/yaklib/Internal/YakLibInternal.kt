package yakstack.yaklib.Internal

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import yakstack.yaklib.Models.CommitModel
import yakstack.yaklib.Models.TaskModel
import yakstack.yaklib.Models.UserModel
import yakstack.yaklib.Obj.ExtendedMsg
import yakstack.yaklib.Obj.ExtendedTaskData
import yakstack.yaklib.Obj.ExtendedTaskIdSet
import yakstack.yaklib.Obj.ExtendedTimeCommitIdSet
import yakstack.yaklib.Obj.ExtendedUserLoginInfo
import yakstack.yaklib.Obj.NULL_MSG
import yakstack.yaklib.Obj.ReturnCode.BAD_DATE_FORMAT
import yakstack.yaklib.Obj.ReturnCode.INVALID_PASSWORD
import yakstack.yaklib.Obj.ReturnCode.INVALID_TOKEN
import yakstack.yaklib.Obj.ReturnCode.SUCCESS
import yakstack.yaklib.Obj.ReturnCode.UNKNOWN_ERROR
import yakstack.yaklib.Obj.ReturnCode.USER_ALREADY_EXISTS
import yakstack.yaklib.Obj.ReturnCode.USER_DOES_NOT_EXIST
import yakstack.yaklib.Obj.TaskData
import yakstack.yaklib.Obj.TaskRequest
import yakstack.yaklib.Obj.TimeCommitData
import yakstack.yaklib.Obj.Token
import yakstack.yaklib.Obj.UserGivenInfo
import java.io.IOException

/**
 * Internal class that controls the networking plane. Handles all responses
 * to and from the Yaklet Webserver.
 */
internal class YakLibInternal(private val endpoint: String) {
    private val taskModel by lazy { initializeTaskModel() }
    private val commitModel by lazy { initializeCommitModel() }
    private val userModel by lazy { initializeUserModel() }

    @JvmSynthetic
    internal fun loginInternal(
        userInfo: UserGivenInfo
    ): ExtendedUserLoginInfo {
        val response = userModel
            .loginUser(
                UserGivenInfo(
                    userInfo.email,
                    userInfo.password,
                    userInfo.device_name
                )
            ).execute()
        val loginInfo = response.body() ?: throw IOException(NULL_MSG)

        return ExtendedUserLoginInfo(when (response.code()) {
            in 200..299 -> SUCCESS
            404 -> USER_DOES_NOT_EXIST
            406 -> INVALID_PASSWORD
            else -> UNKNOWN_ERROR
        }, loginInfo)
    }

    @JvmSynthetic
    internal fun logoutInternal(token: Token, email: String): ExtendedMsg {
        val response = userModel
            .logoutUser(token, email)
            .execute()
        val msg = response.body() ?: throw IOException(NULL_MSG)

        return ExtendedMsg(when (response.code()) {
            in 200..299 -> SUCCESS
            404 -> INVALID_TOKEN
            else -> UNKNOWN_ERROR
        }, msg)
    }

    @JvmSynthetic
    internal fun registerInternal(
        userRegisterInfo: UserGivenInfo
    ): ExtendedMsg {
        val response = userModel
            .registerUser(userRegisterInfo).execute()

        val msg = response.body() ?: throw IOException(NULL_MSG)

        return ExtendedMsg(when (response.code()) {
            in 200..299 -> SUCCESS
            400 -> USER_ALREADY_EXISTS
            else -> UNKNOWN_ERROR
        }, msg)
    }

    @JvmSynthetic
    fun pushTasks(
        taskData: TaskData
    ): ExtendedTaskIdSet {
        val response =
            taskModel
                .pushTask(taskData.asRawTaskData())
                .execute()

        val taskIdSet = response.body() ?: throw IOException(NULL_MSG)
        return ExtendedTaskIdSet(when (response.code()) {
            in 200..299 -> SUCCESS
            401 -> INVALID_TOKEN
            else -> UNKNOWN_ERROR
        }, taskIdSet)
    }

    @JvmSynthetic
    internal fun pushCommitsInternal(
        commitData: TimeCommitData
    ): ExtendedTimeCommitIdSet {
        val response = commitModel
            .pushTimeCommit(commitData.asRawTimeCommitData())
            .execute()

        val timeCommitIdSet = response.body() ?: throw IOException(NULL_MSG)

        return ExtendedTimeCommitIdSet(when (response.code()) {
            in 200..299 -> SUCCESS
            401 -> INVALID_TOKEN
            else -> UNKNOWN_ERROR
        }, timeCommitIdSet)
    }

    @JvmSynthetic
    internal fun getTasksInternal(
        token: Token,
        fat: Boolean
    ): ExtendedTaskData {
        val request = TaskRequest(token, fat)
        val response = if (fat) {
            taskModel
                .getTasksAndMetadata(request).execute()
        } else {
            taskModel
                .getTasks(request).execute()
        }
        val commits = response.body() ?: throw IOException(NULL_MSG)

        return ExtendedTaskData(when (response.code()) {
            in 200..299 -> SUCCESS
            401 -> INVALID_TOKEN
            406 -> BAD_DATE_FORMAT
            else -> UNKNOWN_ERROR
        }, commits.asSendTaskData())
    }

    @JvmSynthetic
    private fun initializeTaskModel(): TaskModel {
        return Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(TaskModel::class.java)
    }

    @JvmSynthetic
    private fun initializeCommitModel(): CommitModel {
        return Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(CommitModel::class.java)
    }

    @JvmSynthetic
    private fun initializeUserModel(): UserModel {
        return Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(UserModel::class.java)
    }
}