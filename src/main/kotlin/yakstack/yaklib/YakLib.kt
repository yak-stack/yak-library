package yakstack.yaklib

import yakstack.yaklib.Internal.YakLibInternal
import yakstack.yaklib.Obj.ExtendedMsg
import yakstack.yaklib.Obj.ExtendedTaskData
import yakstack.yaklib.Obj.ExtendedTaskIdSet
import yakstack.yaklib.Obj.ExtendedTimeCommitIdSet
import yakstack.yaklib.Obj.ExtendedUserLoginInfo
import yakstack.yaklib.Obj.TaskData
import yakstack.yaklib.Obj.TimeCommitData
import yakstack.yaklib.Obj.Token
import yakstack.yaklib.Obj.UserGivenInfo

/**
 * YakLib - a library for interacting with common APIs in the Yak Stack.
 * @param endpoint The root URL where your deployment of the Yak Stack is.
 * This is by default enabled and recommended for all users.
 */
class YakLib(private val endpoint: String) {
    private val internal by lazy { YakLibInternal(endpoint) }

    /**
     * Push tasks user has made to your instance of the YakStack.
     * Note that times in all tasks are expected to be in UTC.
     * Any deviation from this will produce interesting results.
     * @param timeCommits Commits the user has made.
     * @return ReturnCode and Internal TaskIds and PropertyIds of tasks.
     * @since 1.0-ALPHA
     */
    fun pushCommits(timeCommits: TimeCommitData): ExtendedTimeCommitIdSet {
        return internal.pushCommitsInternal(timeCommits)
    }

    /**
     * Logs user in and retrieves an access token that must be used to interact with YakStack service.
     * @param email User's email.
     * @param password User's password.
     * @param deviceName Device name user is logging in from. If null we'll take our best guess.
     * @return ReturnCode and Token user will need to interact with any of the YakStack services.
     * @since 1.0-ALPHA
     */
    @JvmOverloads
    fun login(email: String, password: String, deviceName: String? = null): ExtendedUserLoginInfo {
        val resolvedDeviceName = deviceName ?: "${System.getProperty("user.name")}'s device"
        return login(UserGivenInfo(email, password, resolvedDeviceName))
    }

    /**
     * Logs user in and retrieves an access token that must be used to interact with YakStack service.
     * Only email and password are required. Device name is optional.
     * @param userLoginInfo User's login info.
     * @return ReturnCode and Token user will need to interact with any of the YakStack services.
     * @since 1.0-ALPHA
     */
    fun login(userLoginInfo: UserGivenInfo): ExtendedUserLoginInfo {
        return internal.loginInternal(userLoginInfo)
    }

    /**
     * Logs the user out and ends their user session.
     * @param token User's token.
     * @param email User's email.
     * @return ReturnCode and  Message from server.
     * @since 1.0-ALPHA
     */
    fun logout(token: Token, email: String): ExtendedMsg {
        return internal.logoutInternal(token, email)
    }

    /**
     * Registers the user in the YakStack so they can log in.
     * @param userRegisterInfo Info to register user.
     * @return ReturnCode and Message from the server.
     * @since 1.0-ALPHA
     */
    fun register(
        userRegisterInfo: UserGivenInfo
    ): ExtendedMsg {
        return internal.registerInternal(userRegisterInfo)
    }

    /**
     * Registers the user in the YakStack so they can log in.
     * @param email User's email.
     * @param firstName User's first name.
     * @param lastName User's last name.
     * @param password: User's password to be set.
     * @return ReturnCode and msg from the server.
     * @since 1.0-ALPHA
     */
    fun register(
        email: String,
        firstName: String,
        lastName: String,
        password: String
    ): ExtendedMsg {
        return register(UserGivenInfo(email, firstName, lastName, password))
    }

    /**
     * Push tasks up to your instance of the YakStack.
     * Tasks can include TimeCommits as well, allowing for easy bulk operations.
     * @param taskData User's task data.
     */
    fun pushTasks(
        taskData: TaskData
    ): ExtendedTaskIdSet {
        return internal.pushTasks(taskData)
    }

    /**
     * Get all tasks user has made with no extra metadata.
     * @param token User token.
     */
    fun getTasks(
        token: Token
    ): ExtendedTaskData {
        return internal.getTasksInternal(token, false)
    }

    /**
     * Get all tasks user has made and all metadata with tasks.
     * @param token User token.
     * @return ExtendedTaskData ReturnCode and TaskData for all requested tasks.
     * @since 1.0-ALPHA
     */
    fun getTasksWithMetadata(
        token: Token
    ): ExtendedTaskData {
        return internal.getTasksInternal(token, true)
    }
}
