# DISCONTINUED

The YakStack is dropping support for the YakLib. We are now treating Protobufs
as a first class citizen for interacting with the Yaklet Webserver and other
components of the YakStack.

For relevant Protobufs, see the [ProtoYak repo](https://gitlab.com/yak-stack/proto-yak).

No guarantees are made about the YakLibrary and may stop working at any point in
the future. We *strongly* do not recommend using it.

-------

# Welcome to the Yak Library (ALPHA PROJECT)

*DO NOT COME HERE EXPECTING ANYTHING TO BE STABLE. THIS IS NOT A USABLE PRODUCT
RIGHT NOW AND SHOULD BE TREATED AS SUCH.*

Yak Library, or YakLib for short is an officially supported library written on
Kotlin to interface with the Yak Stack.

Within YakLib are functions to interface with all of the endpoints so you don't
have to.

# Include me :D

Using gradle? If you are, include jitpack in your repos, and then include the
version of the library you're looking for.

[![](https://jitpack.io/v/com.gitlab.yak-stack/yak-library.svg)](https://jitpack.io/#com.gitlab.yak-stack/yak-library)

```
repositories {
    maven { url 'https://jitpack.io' }
}

dependencies {
    compile 'com.gitlab.yak-stack:yak-library:0.7.7-ALPHA'
}
```

All releases are specified as tags! So be sure to check there to see what the
latest version is! I'll do my best to keep the README up to date though :)

# Docs

Currently the only docs are hosted on GitLab pages.

For the current docs, check [here](https://yak-stack.gitlab.io/yak-library/yaklib/yakstack.yaklib/-yak-lib/index.html)!

# Building

```
./gradlew shadowJar
```

As it is, the only dependency for building is Java, although this may go away in
the future.

Your fancy new jar will appear somewhere to be updated later when it's actually
built.

## Copying

Yak Library is licensed under LGPL v3.0. Include it in any of your programs, but
if you make any changes you must contribute back.
